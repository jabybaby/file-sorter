let express = require('express');
let app = express();
const config = require('./config.json');
let done = require('./done.json');
const fs = require("fs");
const port = 4000
let files = [];

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/', express.static(config.root))

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
	files = files.filter(el => Object.keys(done).indexOf(el) === -1);
	res.render('index', {file: shuffle(files)[0], categories: config.categories, count: files.length});
});

app.post("/set", (req, res) => {
	done[req.body.file] = req.body.category;
	fs.writeFile("done.json", JSON.stringify(done), () => {});
	res.redirect("/");
})

app.get("/move", async (req, res) => {

	await Promise.all(
		config.categories.map(cat => {
			return new Promise((resolve, reject) => {
				fs.mkdir(config.root + cat, { recursive: true }, (err) => {resolve()});
			})
		})
	)

	Object.keys(done).forEach(el => {
		let path = config.root + el;
		let dest = config.root + done[el] + "/" + el.split("/").pop();
		fs.rename(path, dest, (err) => {
			if (err) {
				console.log(err);
			}
		});
	})

	res.redirect("/");
});

app.listen(port, async () => {
	files = await(traverseFolder(config.root));
});

function traverseFolder(root, first = true)
{
	return new Promise((resolve) => {
		fs.readdir(root, async (err, files) => {
			if (err) {
				console.log(err);
				return;
			}
			files = files.map(file => {
				return new Promise((res2) => {
					let path = root + '/' + file;
					fs.stat(path, (err, stats) => {
						if (err) {
							console.log(err);
							return;
						}
						if (stats.isDirectory()) {
							return res2(traverseFolder(path, false));
						} else {
							return res2(path);
						}
					});
				})
			});

			files = await Promise.all(files);
			resolve(files.flat().map(el => first?el.replace(root, ""):el));
		});
	});
}

function shuffle(array) {
	return array.sort(() => Math.random() - 0.5);
}